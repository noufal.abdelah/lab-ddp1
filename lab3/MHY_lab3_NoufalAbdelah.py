from random import randint

# ini teks yang bakalan kita taro enkripsi
paragraf = "the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. the quick brown fox jumped over the lazy dog's back. waltz, nymph, for quick jigs vex bud. sphinx of black quartz, judge my vow. pack my box with five dozen liquor jugs. glib jocks quiz nymph to vex dwarf. jackdaws love my big sphinx of quartz. the five boxing wizards jump quickly. how vexingly quick daft zebras jump! quick zephyrs blow, vexing daft jim. two driven jocks help fax my big quiz. the jay, pig, fox, zebra and my wolves quack! sympathizing would fix quaker objectives. a wizards job is to vex chumps quickly in fog. watch jeopardy!, alex trebek s fun tv quiz game. by jove, my quick study of lexicography won a prize! waxy and quivering, jocks fumble the pizza. "


# ini list yang isinya a sampe z
list_huruf = [i for i in "abcdefghijklmnopqrstuvwxyz"]

# minta input kode yang mau dienkripsi
input_enkripsi = input("Masukkan pesan yang akan dikirim: \n")

# minta input n
input_n = int(input("Pilih nilai n: "))

# angka genap sama ganjil
genap = [0,2,4,6,8]
ganjil = [1,3,5,7,9]



# ngeenkripsi input sejauh n, jadi misalkan n = 5, input = "abc de"
# setelah dienkripsi jadi "fgh ij"

# ini pertama cek dulu, character yang dimasukin ke input itu ada didalem list huruf atau ngga
# kalo ada (berarti dia huruf), dia bakal digeser kayak yg diatas
# kalo gaada , kita cek dulu dia itu angka atau bukan
# kalo angka, dia digeser sesuai sifatnya (ganjil ato genap)
# kalo bukan angka, dia ga berubah (misalkan spasi, tanda tanya, tanda seru)

encrypted = ""

for char in input_enkripsi:
    if char.lower() in list_huruf:
        encrypted += list_huruf[((list_huruf.index(char.lower()) + input_n)% 26)]
    else: 
        if char.isdigit():
            if int(char) % 2 == 0:
                enc_char = str(genap[(genap.index(int(char) )+ input_n)% 5])
            else:
                enc_char = str(ganjil[(ganjil.index(int(char) )+ input_n)% 5])
            encrypted += enc_char
        else:
            encrypted += char


# ini ada di ketentuan 4 di soal
encrypt_key = '*' + str(20220310-input_n) + "*"
posisi_encrypt_key = randint(0,len(paragraf))



# count buat ngitung udah berapa kali iterasi
count = 0

# ini buat nampung paragraf yang udah ditambahin enkripsi
result = ""

# ini index dari input lu
index = 0

# ini temp buat nampung enkripsi lu yang berupa angka
temp = ""


# iterasi untuk setiap character di paragraf
for char in paragraf:
    
    # naro encrypt_key secara random
    if count == posisi_encrypt_key:
        result += encrypt_key

    # iterasi dengan syarat indexnya kurang dari panjang input
    if index < len(encrypted):

        # cek char paragraf yang sedang diiterasi itu sama dengan character yang ada di encrypted[index]
        if char == encrypted[index]:

            # cek kalo ini index terakhir atau bukan
            if index != len(encrypted)-1:

                # kalo misalkan character yang lagi diiterasi itu "b" dan character setelahnya itu spasi
                # khusus buat spasi dia diganti jadi "$"
                if encrypted[index+1] == " ":
                    result += char.upper()
                    result += "$"
                else:
                    result += char.upper()
                index += 1
        # ini misalkan char paragrafnya itu ternyata ga sama dengan character encrypted[index]
        else:
            # cek kalo misalkan dia angka
            if encrypted[index].isdigit():
                result += encrypted[index]
                # kalo dia angka bakal ngeappend string temp tadi
                temp += char
                index += 1
            else:
                # nambahin tempnya sama character selanjutnya ke dalem result
                result += temp
                result += char
                temp = ""
    else: 
        # nambahin tempnya sama character selanjutnya ke dalem result
        result += temp
        result += char
        temp = ""
    # nambahin itungan berapa kali iterasi
    count += 1

print(result)


