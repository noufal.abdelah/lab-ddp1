print("""==================================
WELCOME TO KARTI-NIAN CHALLENGE
==================================
""")

jumlah_acara = int(input("Masukkan jumlah acara yang akan dilombakan: "))
dict_lomba = {}
for i in range(1,jumlah_acara+1):
    
    print(f"\nLOMBA {i}")
    nama_lomba = input("Nama lomba: ")
    list_hadiah = []
    for i in range(1,4):
        hadiah = int(input(f"Hadiah juara {i}: "))
        list_hadiah.append(hadiah)
    dict_lomba[nama_lomba] = [list_hadiah]
    


print("""
==================================
DATA MATA LOMBA DAN HADIAH
==================================
""")

for lomba,keterangan in dict_lomba.items():
    print(f"\nLomba {lomba}")
    for i in range(0,len(keterangan[0])):
        print(f"[Juara {i+1}] {keterangan[0][i]} ")
    

print("""
==================================
DATA PESERTA CHALLENGE
==================================
""")

jumlah_peserta = int(input("Masukkan jumlah peserta yang berpartisipasi: "))
print("\n")
list_peserta = []
list_peserta_lower = []
for i in range(1,jumlah_peserta+1):
    nama_peserta = input(f"Nama Peserta {i}: ")
    if nama_peserta.lower() not in list_peserta_lower:
        list_peserta.append(nama_peserta)
        list_peserta_lower.append(nama_peserta.lower())
       
list_peserta.sort()

print("""
==================================
CHALLENGE DIMULAI
==================================
""")

for lomba in dict_lomba.keys():
    list_juara = []
    print(f"Lomba {lomba}")
    for i in range(1,4):
        juara = input(f"Juara {i}: ")
        list_juara.append(juara.lower())
    print("\n")
    dict_lomba[lomba].append(list_juara)



print("""
==================================
FINAL RESULT
==================================
""")

for peserta in list_peserta:
    print(peserta)
    daftar_juara_yang_diraih = []
    total_hadiah = 0
    for lomba, keterangan in dict_lomba.items():
        if peserta.lower() in keterangan[1]:
            juara_yang_diraih = keterangan[1].index(peserta.lower())
            daftar_juara_yang_diraih.append(juara_yang_diraih+1) 
            total_hadiah += keterangan[0][juara_yang_diraih]

    output_juara = "Juara yang pernah diraih: "
    for i in range(1,len(daftar_juara_yang_diraih)+1):
        if i != len(daftar_juara_yang_diraih):
            output_juara += str(i) + ", "
        else:
            output_juara += str(i) + "\n"

    print(f"Total hadiah: {total_hadiah}")
    print(output_juara)